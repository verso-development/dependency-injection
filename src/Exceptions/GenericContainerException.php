<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace DI\Exceptions;

use Psr\Container\ContainerExceptionInterface;
use RuntimeException;

class GenericContainerException extends RuntimeException implements ContainerExceptionInterface
{
    /**
     * GenericContainerException constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct('An error has occurred while resolving a dependency : ' . $message);
    }
}
