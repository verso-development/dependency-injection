<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace DI\Exceptions;

class NonTypedFieldException extends GenericContainerException
{
    public function __construct()
    {
        parent::__construct("Non typed field");
    }
}
