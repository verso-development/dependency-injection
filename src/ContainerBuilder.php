<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace DI;

use Psr\Container\ContainerInterface;

class ContainerBuilder
{
    private AbstractBinder $binder;

    private bool $autoWiring = false;

    private function __construct(AbstractBinder $binder)
    {
        $this->binder = $binder;
    }

    /**
     * ContainerBuilder constructor.
     * @param AbstractBinder $binder
     * @return ContainerBuilder
     */
    public static function new(AbstractBinder $binder)
    {
        return new ContainerBuilder($binder);
    }

    /**
     * @return ContainerBuilder
     */
    public function withAutoWiring(): self
    {
        $this->autoWiring = true;

        return $this;
    }

    public function build(): ContainerInterface
    {
        $this->binder->configure();

        return new Container($this->binder->getDependencies(), $this->getOptions());
    }

    private function getOptions(): array
    {
        return [
            'autoWiring' => $this->autoWiring
        ];
    }
}
