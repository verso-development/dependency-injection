<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace DI;

use DI\Mappers\ClassMapper;
use DI\Mappers\Mapper;
use InvalidArgumentException;

abstract class AbstractBinder
{
    /**
     * @var Mapper[]
     */
    private array $dependencies = [];

    abstract public function configure(): void;

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return $this->dependencies;
    }

    protected function bindToInstance(string $definition, object $instance): void
    {
        if (!class_exists($definition) && !interface_exists($definition)) {
            throw new InvalidArgumentException(sprintf("%s must exist", $definition));
        }

        if (key_exists($definition, $this->dependencies)) {
            throw new InvalidArgumentException(
                sprintf(
                    "%s has already be bonded with %s",
                    $definition,
                    $this->dependencies[$definition]
                )
            );
        }

        $this->dependencies[$definition] = new Mapper($instance);
    }

    protected function bindToScalar(string $definition, $value): void
    {
        if (key_exists($definition, $this->dependencies)) {
            throw new InvalidArgumentException(
                sprintf("%s has already be bonded with %s", $definition, $this->dependencies[$definition])
            );
        }

        $this->dependencies[$definition] = new Mapper($value);
    }

    protected function bind(string $definition, string $implementation): void
    {
        if (!(class_exists($definition) && interface_exists($definition)) && !class_exists($implementation)) {
            throw new InvalidArgumentException(sprintf("%s and %s must exist", $definition, $implementation));
        }

        if (key_exists($definition, $this->dependencies)) {
            throw new InvalidArgumentException(
                sprintf(
                    "%s has already be bonded with %s",
                    $definition,
                    $this->dependencies[$definition]
                )
            );
        }

        $this->dependencies[$definition] = new ClassMapper($implementation);
    }

    protected function bindAsSingleton(string $definition, string $implementation): void
    {
        if (!(class_exists($definition) && interface_exists($definition)) && !class_exists($implementation)) {
            throw new InvalidArgumentException(sprintf("%s and %s must exist", $definition, $implementation));
        }

        if (key_exists($definition, $this->dependencies)) {
            throw new InvalidArgumentException(
                sprintf(
                    "%s has already be bonded with %s",
                    $definition,
                    $this->dependencies[$definition]
                )
            );
        }

        $this->dependencies[$definition] = new ClassMapper($implementation, true);
    }
}
