<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace DI;

use DI\Exceptions\GenericContainerException;
use DI\Exceptions\NonInstantiableClassException;
use DI\Exceptions\NonTypedFieldException;
use DI\Exceptions\NotFoundException;
use DI\Mappers\ClassMapper;
use DI\Mappers\Mapper;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;

class Resolver
{
    /**
     * @var Mapper[]
     */
    private array $binds;

    /**
     * @var array
     */
    private array $dependencies;

    /**
     * Resolver constructor.
     * @param Mapper[] $binds
     */
    public function __construct(array $binds)
    {
        $this->binds = $binds;
        $this->dependencies = [];

        foreach ($binds as $id => $bind) {
            if (!($bind instanceof ClassMapper)) {
                $value = $bind->getValue();

                // Add the scalar value or the object instance on dependencies
                $this->dependencies[$id] = $value;
            }
        }
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function resolve(string $id)
    {
        if (array_key_exists($id, $this->dependencies)) {
            return $this->dependencies[$id];
        } else {
            if (!class_exists($id) && !interface_exists($id)) {
                throw new NotFoundException();
            }

            try {
                $implementation = array_key_exists($id, $this->binds) ? $this->binds[$id]->getValue() : $id;
                $class = new ReflectionClass($implementation);

                if (!$class->isInstantiable()) {
                    throw new NonInstantiableClassException();
                }

                $constructor = $class->getConstructor();
                $parameters = [];

                if ($constructor) {
                    foreach ($constructor->getParameters() as $parameter) {
                        $parameterType = $parameter->getType();

                        if (!$parameterType && !($parameterType instanceof ReflectionNamedType)) {
                            throw new NonTypedFieldException();
                        }

                        $parameters[] = $this->resolve($parameterType->getName());
                    }
                }

                $instance = $class->newInstance(...$parameters);

                if (array_key_exists($id, $this->binds)) {
                    $mapper = $this->binds[$id];

                    if ($mapper instanceof ClassMapper && $mapper->isSingleton()) {
                        // Put the instance on the dependencies array if the mapper is marked as a singleton
                        $this->dependencies[$id] = $instance;
                    }
                }

                return $instance;
            } catch (ReflectionException $exception) {
                throw new GenericContainerException($exception->getMessage());
            }
        }
    }
}
