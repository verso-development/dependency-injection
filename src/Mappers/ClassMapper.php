<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace DI\Mappers;

class ClassMapper extends Mapper
{
    private bool $asSingleton;

    /**
     * ClassMapper constructor.
     * @param $value
     * @param bool $asSingleton
     */
    public function __construct($value, bool $asSingleton = false)
    {
        parent::__construct($value);

        $this->asSingleton = $asSingleton;
    }

    /**
     * @return bool
     */
    public function isSingleton(): bool
    {
        return $this->asSingleton;
    }
}
