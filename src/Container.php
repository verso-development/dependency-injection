<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace DI;

use DI\Exceptions\NotFoundException;
use DI\Mappers\Mapper;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;

class Container implements ContainerInterface
{
    /**
     * @var Mapper[]
     */
    private array $binds;

    /**
     * @var array
     */
    private array $options;

    /**
     * @var Resolver
     */
    private Resolver $resolver;

    /**
     * Container constructor.
     * @param array $binds
     * @param array $options
     */
    public function __construct(array $binds, array $options = [])
    {
        $binds[ContainerInterface::class] = new Mapper($this); // Add the container

        $this->binds = $binds;
        $this->options = $options;

        $this->resolver = new Resolver($binds);
    }

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->resolver->resolve($id);
        } else {
            if ($this->options['autoWiring']) {
                // Dynamical resolving
                return $this->resolver->resolve($id);
            } else {
                throw new NotFoundException();
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        if (!is_string($id)) {
            throw new InvalidArgumentException('Id must be a string');
        }

        return array_key_exists($id, $this->binds);
    }
}
