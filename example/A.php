<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Example;

class A implements IA
{

    /**
     * A constructor.
     * @param B $b
     */
    public function __construct(B $b)
    {
        $b->sayHello();
    }

    public function sayHello(): void
    {
        echo 'Hello from A !' . PHP_EOL . PHP_EOL;
    }
}
