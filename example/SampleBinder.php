<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Example;

use DI\AbstractBinder;

class SampleBinder extends AbstractBinder
{
    public function configure(): void
    {
        $this->bind(IC::class, C::class);
        $this->bindToInstance(B::class, new B());
        $this->bindToScalar('db.host', 'localhost');
        $this->bindAsSingleton(IA::class, A::class);
    }
}
