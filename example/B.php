<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Example;

class B
{
    public function sayHello()
    {
        echo 'Hello from B !' . PHP_EOL . PHP_EOL;
    }
}
