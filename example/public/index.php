<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

use DI\ContainerBuilder;
use Example\B;
use Example\IA;
use Example\IC;
use Example\SampleBinder;

require '../../vendor/autoload.php';
require '../functions.php';

$container = ContainerBuilder::new(new SampleBinder())
    ->withAutoWiring()
    ->build();

debug($container->get(IC::class));

debug($container->get(B::class));

// Single instance because IA was defined as singleton
debug($container->get(IA::class));

// New instance because IC was not defined as singleton
debug($container->get(IC::class));

debug($container->get('db.host'));
